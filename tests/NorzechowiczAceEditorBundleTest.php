<?php

declare(strict_types=1);

namespace Flagstone\AceEditorBundle\Tests;

use Flagstone\AceEditorBundle\DependencyInjection\Compiler\TwigFormPass;
use Flagstone\AceEditorBundle\FlagstoneAceEditorBundle;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FlagstoneAceEditorBundleTest extends TestCase
{
    public function testBuild()
    {
        $container = new ContainerBuilder();
        $bundle = new FlagstoneAceEditorBundle();
        $bundle->build($container);

        $this->assertNotEmpty(array_filter(
            $container->getCompilerPassConfig()->getPasses(),
            function ($value) {
                return $value instanceof TwigFormPass;
            }
        ));
    }
}
